package example

import (
	"time"
	"fmt"
)

func ShowTime() {
	fmt.Printf("The current time is %v\n", time.Now())
}
